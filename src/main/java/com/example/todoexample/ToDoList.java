package com.example.todoexample;

import com.vaadin.spring.annotation.SpringComponent;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.VerticalLayout;
import jdk.nashorn.internal.objects.annotations.Constructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@UIScope
@SpringComponent
public class ToDoList extends VerticalLayout implements ToDoChangerListener {

    @Autowired
    ToDoRepository repository;
    private List<ToDo> todos;

    @PostConstruct
    void init() {
        setWidth("100%");
        setTodos(repository.findAll());
    }

    private void setTodos(List<ToDo> todos) {
        this.todos = todos;

        removeAllComponents();

        todos.forEach(toDo -> addComponent(new ToDoLayout(toDo,this)));

    }

    public void addTodo(ToDo toDo) {
        repository.save(toDo);
        update();
    }

    private void update() {
        setTodos(repository.findAll());
    }

    public void delCompleated() {
        repository.deleteByDone(true);
        update();
    }

    @Override
    public void toDoChanged(ToDo toDo) {
        addTodo(toDo);
    }
}

