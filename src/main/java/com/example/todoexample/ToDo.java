package com.example.todoexample;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class ToDo {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String text;

    private boolean done;

    private String dateField;

    public ToDo() {
    }

//
//    public ToDo(String text, boolean done, String dateField) {
//        this.text = text;
//        this.done = done;
//        this.dateField = dateField;
//    }

    public ToDo(String text, String dateField) {
        this.text = text;
        this.dateField = dateField;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public String getDateField() {
        return dateField;
    }

    public void setDateField(String dateField) {
        this.dateField = dateField;
    }

    public void toggleDone() {
        setDone(!isDone());
    }


}
