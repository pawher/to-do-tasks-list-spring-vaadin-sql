package com.example.todoexample;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.transaction.annotation.Transactional;

public interface ToDoRepository extends JpaRepository<ToDo, Long> {
    @Transactional
    void deleteByDone(boolean b);

}
