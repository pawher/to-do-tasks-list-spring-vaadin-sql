package com.example.todoexample;

import com.vaadin.annotations.Theme;
import com.vaadin.event.ShortcutAction;
import com.vaadin.server.VaadinRequest;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import org.springframework.beans.factory.annotation.Autowired;

import java.text.DateFormat;
import java.time.LocalDate;

@SpringUI
@Theme("valo")
public class ToDoUI extends UI {

    private VerticalLayout layout;

    @Autowired
    ToDoList toDoList;

    @Override
    protected void init(VaadinRequest vaadinRequest) {
        setupLayout();
        addHeader();
        addForm();
        addToDoList();
        addActionButton();

    }

    private void setupLayout() {
        layout = new VerticalLayout();
        layout.setDefaultComponentAlignment(Alignment.MIDDLE_CENTER);
        setContent(layout);
    }

    private void addHeader() {
        Label header = new Label("ToDO");
        header.addStyleName(ValoTheme.LABEL_H1);
        header.setSizeUndefined();
        layout.addComponent(header);

    }

    private void addForm() {
        HorizontalLayout horizontalLayout = new HorizontalLayout();
        horizontalLayout.setSpacing(true);
        horizontalLayout.setWidth("80%");

        TextField taskField = new TextField();
        taskField.setWidth("100%");

        DateField dateField = new DateField();
//        TextField dateField = new TextField();


        Button add = new Button("Add");
        add.setStyleName(ValoTheme.BUTTON_PRIMARY);


        add.addClickListener(clickEvent -> {
            toDoList.addTodo(new ToDo(taskField.getValue(),dateField.getValue().toString()));
            taskField.setValue("");
            taskField.focus();
//            dateField.setValue("");
            dateField.focus();
        });
        add.setClickShortcut(ShortcutAction.KeyCode.ENTER);

        horizontalLayout.addComponents(taskField, dateField, add);
        horizontalLayout.setExpandRatio(taskField, 1);
        layout.addComponent(horizontalLayout);
    }

    private void addToDoList() {

        toDoList.setWidth("80%");
        layout.addComponent(toDoList);

    }

    private void addActionButton() {
        Button delateButton = new Button("Delate compleated");
        delateButton.setStyleName(ValoTheme.BUTTON_DANGER);

        delateButton.addClickListener(clickEvent -> {
            toDoList.delCompleated();
        });


        layout.addComponent(delateButton);

    }
}
