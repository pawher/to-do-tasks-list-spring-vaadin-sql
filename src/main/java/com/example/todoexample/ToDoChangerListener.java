package com.example.todoexample;

public interface ToDoChangerListener {
    void toDoChanged (ToDo toDo);
}
