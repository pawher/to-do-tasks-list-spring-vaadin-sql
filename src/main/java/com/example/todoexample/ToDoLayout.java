package com.example.todoexample;

import com.vaadin.shared.ui.ValueChangeMode;
import com.vaadin.ui.*;
import com.vaadin.ui.themes.ValoTheme;
import com.vaadin.data.Binder;


public class ToDoLayout extends HorizontalLayout {

    private final CheckBox done;
    private final TextField text;
    private final TextField dateText;


    public ToDoLayout(ToDo todo, ToDoChangerListener changerListener) {

        setWidth("100%");
        setDefaultComponentAlignment(Alignment.MIDDLE_LEFT);

        done = new CheckBox();
        text = new TextField();
        dateText = new TextField("");

        text.setValue(todo.getText());
        text.addStyleName(ValoTheme.TEXTAREA_BORDERLESS);
        text.setValueChangeMode(ValueChangeMode.BLUR);

        dateText.setValue(todo.getDateField());
        dateText.addStyleName(ValoTheme.TEXTAREA_BORDERLESS);
        dateText.setValueChangeMode(ValueChangeMode.BLUR);


        Binder<ToDo> binder = new Binder<>(ToDo.class);
        binder.bindInstanceFields(this);
        binder.setBean(todo);

        addComponent(done);
        addComponentsAndExpand(text,dateText);

        binder.addValueChangeListener(valueChangeEvent -> changerListener.toDoChanged(todo));

    }
}
